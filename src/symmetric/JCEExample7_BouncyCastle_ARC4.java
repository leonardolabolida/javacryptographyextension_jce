package symmetric;

import java.security.Provider;
import java.security.Security;
import java.util.Enumeration;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class JCEExample7_BouncyCastle_ARC4 {
	
	public static void main(String[] args) {
		try {
			/*
			PROVIDER: BOUNCY CASTLE BC
			KeyGenerator.Threefish-1024
			AlgorithmParameters.Threefish-1024
			Cipher.Threefish-1024
			*/
			Security.insertProviderAt( new BouncyCastleProvider() , 0 ); // bcprov-jdk14-119.jar
			
	        byte message[] = "This is the message that I want to keep secret".getBytes() ;
	        
	        // GENERATE SYMMETRIC KEY 
	        KeyGenerator keygenerator = KeyGenerator.getInstance("ARC4","BC");
	        SecretKey secretKey = keygenerator.generateKey();
	        System.out.println( " KEY_Algorithm:" + secretKey.getAlgorithm() );
	        System.out.println( " secretKey.getEncoded:" + new String(secretKey.getEncoded()) );
	        
	        // ENCODE
	        Cipher cipher = Cipher.getInstance("ARC4" , "BC");
	        cipher.init( Cipher.ENCRYPT_MODE, secretKey);
	        byte result[] = cipher.doFinal(message);
	        System.out.println("result:" + new String(result) );
	        
	        // DECODE
	        cipher.init( Cipher.DECRYPT_MODE, secretKey);
	        byte msg[] = cipher.doFinal(result);
	        System.out.println("result2:" + new String(msg) );
	        
		}
		catch (Exception e) {
			System.out.println("error at main():" + e.getMessage() );
		}
	}
	
}
