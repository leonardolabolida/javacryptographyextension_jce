package symmetric;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

/** SIGNATURE
	key:Alg.Alias.Signature.SHA256/DSA
*/
public class JCEExample8_BouncyCastle_Signature {
	
	public static void main(String[] args) {
		try {
			Security.insertProviderAt( new BouncyCastleProvider() , 0 ); // bcprov-jdk14-119.jar
			
	        byte message[] = "This is the message that I want to sign".getBytes() ;
	        
	        // GENERATE PUBLIC AND PRIVATE KEYS   OPTIONS: DiffieHellman,DSA,RSA,EC
	        KeyPairGenerator KeyPairGenerator = java.security.KeyPairGenerator.getInstance("DSA");
	        KeyPair keyPair = KeyPairGenerator.genKeyPair();
	        PublicKey publicKey = keyPair.getPublic();
	        PrivateKey privateKey = keyPair.getPrivate();
	        System.out.println( " publicKey:" + new String(publicKey.getEncoded()) );
	        System.out.println( " privateKey:" + new String(privateKey.getEncoded()) );

	        // SIGNATURE
	        Signature signature = Signature.getInstance("SHA256/DSA");
	        signature.initSign( privateKey );
	        signature.update(message);
	        byte[] sign = signature.sign();
	        System.out.println("sign:"+ new String(sign) );
	        
	        // VALIDATE VERIFY
	        Signature signature2 = Signature.getInstance("SHA256/DSA");
	        signature2.initVerify(publicKey);
	        signature2.update(message);
	        boolean verify = signature2.verify(sign);
	        if (verify) {
	        	System.out.println("Signature verify OK");
	        }else{
	        	System.out.println("Signature verify ERROR");
	        }
	        
	        
		}
		catch (Exception e) {
			System.out.println("error at main():" + e.getMessage() );
		}
	}
	
}
