package symmetric;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;

public class JCEExample2_RSA {
	
	public static void main(String[] args) {
		try {
	        byte message[] = "This is the message that I want to keep secret".getBytes() ;
	        
	        // GENERATE PUBLIC AND PRIVATE KEYS   OPTIONS: DiffieHellman,DSA,RSA,EC
	        KeyPairGenerator KeyPairGenerator = java.security.KeyPairGenerator.getInstance("RSA");
	        KeyPair keyPair = KeyPairGenerator.genKeyPair();
	        PublicKey publicKey = keyPair.getPublic();
	        PrivateKey privateKey = keyPair.getPrivate();
	        System.out.println( " publicKey:" + new String(publicKey.getEncoded()) );
	        System.out.println( " privateKey:" + new String(privateKey.getEncoded()) );
	        
	        // ENCODE
	        Cipher cipher = Cipher.getInstance("RSA" , "SunJCE");
	        cipher.init( Cipher.ENCRYPT_MODE, publicKey);
	        byte result[] = cipher.doFinal(message);
	        System.out.println("result:" + new String(result) );
	        
	        // DECODE
	        cipher.init( Cipher.DECRYPT_MODE, privateKey);
	        byte msg[] = cipher.doFinal(result);
	        System.out.println("result2:" + new String(msg) );
	        
		}
		catch (Exception e) {
			System.out.println("error at main():" + e.getMessage() );
		}
	}
	
}
