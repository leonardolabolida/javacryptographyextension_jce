package symmetric;


/**
 * Generates binary password (256 - using all ASCII)
 */
public class GenKeyCrack2 {

	/***********************************************
	 * @Test 
	 */
	public static void main(String[] args) {
		try {
			
			//byte[] key = "aa".getBytes(); // Initial key
			byte[] key = { (byte)0 , (byte)255 , (byte)126 };
			
			for ( int gen=0; gen<250; gen++) {
				getNextKey( key , key.length-1 ) ; // key.length-1 = last byte
				printKey(key);
			}
			
		}
		catch (Exception e) {
			System.out.println("error at main():" + e.getMessage());
		}
	}

	/***********************************************
	 * getNextKey 
	 */
	private static void getNextKey( byte[] key , int pos ) {
		int i = key[pos];
		if (i<0) i=i+256;
		if ( i<255 ) {
			key[pos]++;
		}else{
			key[pos]=0;
			getNextKey( key , pos-1 );
		}
	}
	
	/***********************************************
	 * printKey 
	 */
	private static void printKey( byte[] key ) {
		for ( int x=0 ; x<key.length ; x++ ) {
			if ( key[x]<0 ) {
				System.out.print( (key[x]+256) + " " );
			}else{
				System.out.print( key[x] + " " );
			}
		}
		System.out.print("-->");
		for ( int x=0 ; x<key.length ; x++ ) {
			System.out.print( (char)key[x]);
		}
		System.out.print("\n");
	}
	
}
