package symmetric;

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import org.bouncycastle.jce.provider.BouncyCastleProvider;


/******************************************************************************************
 * JCEExample90_Mix by Leonardo Labolida
 * A mix of cryptographic methods
 */
public class JCEExample90_Mix {
	
	/******************************************************************************************
	 * Test 
	 */
	public static void main(String[] args) {
		try {
			new JCEExample90_Mix().exec();
		}
		catch (Exception e) {
			System.out.println("error at main():" + e.getMessage() );
		}
	}
	
	/******************************************************************************************
	 * execute 
	 */
	public void exec() throws Exception{
		try {
			
			byte message[] = "This is my secret message".getBytes() ;
			
			Security.insertProviderAt( new BouncyCastleProvider() , 0 ); // bcprov-jdk14-119.jar
			
			// Public and Private keys DSA
			KeyPair keyPair = cryptography_generate_key_pair("RSA");
	        PublicKey publicKey = keyPair.getPublic();
	        PrivateKey privateKey = keyPair.getPrivate();
	        
	        byte b64publicKey[] = Utils.base64Encode( publicKey.getEncoded() );   // BASE64
	        byte b64privateKey[] = Utils.base64Encode( privateKey.getEncoded() ); // BASE64
	        Utils.fileWrite("b64publicKey.key", b64publicKey);                     // Save Key
	        Utils.fileWrite("b64privateKey.key", b64privateKey);                   // Save Key
	        
	        // symmetric_key  DES
			SecretKey symmetric_key_DES = cryptography_generate_key_symmetric("DES");
			
			byte b64SymmetricKey[] = Utils.base64Encode( symmetric_key_DES.getEncoded() ); // BASE64
			Utils.fileWrite("b64SymmetricKeyDES.key", b64SymmetricKey);                   // Save Key
			
			
			//ARC4
			SecretKey symmetric_key_ARC4 = cryptography_generate_key_symmetric("ARC4");
			byte b64SymmetricKeyARC4[] = Utils.base64Encode( symmetric_key_ARC4.getEncoded() ); // BASE64
			Utils.fileWrite("b64SymmetricKeyARC4.key", b64SymmetricKeyARC4);                   // Save Key
			
			
			
			
			// ENCODE
			
			// Encode symmetric DES
			message = cryptography_encode( "DES", symmetric_key_DES , message );
			System.out.println("message=" + new String(message) );
			
			// Encode asymmetric DSA
			message = cryptography_encode( "RSA", publicKey , message );
			System.out.println("message=" + new String(message) );
			
			// Encode symmetric AES
			message = cryptography_encode( "ARC4", symmetric_key_ARC4 , message );
			System.out.println("message=" + new String(message) );
			
			
			
			
			// DECODE
			
			// Decode Symmetric AES
			message = cryptography_decode( "ARC4", symmetric_key_ARC4 , message );
			System.out.println("message=" + new String(message) );
			
			
			// Decode asymmetric DSA
			message = cryptography_decode( "RSA", privateKey , message );
			System.out.println("message=" + new String(message) );
			
			
			// Decodesymmetric DES
			message = cryptography_decode( "DES", symmetric_key_DES , message );
			System.out.println("message=" + new String(message) );
			
			
			
		}
		catch (Exception e) {
			throw new Exception("error at exec():" + e.getMessage() );
		}
	}
	
	/******************************************************************************************
	 * cryptography_encode
	 */
	public static KeyPair cryptography_generate_key_pair( String algorithm  ) throws Exception{
		try {
	        KeyPairGenerator KeyPairGenerator = java.security.KeyPairGenerator.getInstance(algorithm);
	        KeyPair keyPair = KeyPairGenerator.genKeyPair();
	        PublicKey publicKey = keyPair.getPublic();
	        PrivateKey privateKey = keyPair.getPrivate();
	        return keyPair;
		}
		catch (Exception e) {
			throw new Exception("error at cryptography_generate_key_pair():" + e.getMessage() );
		}
	}
	
	/******************************************************************************************
	 * cryptography_encode
	 */
	public static SecretKey cryptography_generate_key_symmetric( String algorithm  ) throws Exception {
		try {
	        KeyGenerator keygenerator = KeyGenerator.getInstance(algorithm, "BC");
	        SecretKey secretKey = keygenerator.generateKey();
	        return secretKey ;
		}
		catch (Exception e) {
			throw new Exception("error at cryptography_generate_key_symmetric():" + e.getMessage() );
		}
	}
	
	/******************************************************************************************
	 * cryptography_encode
	 * @param Key (KeyPair||SecretKey)
	 */
	public static byte[] cryptography_encode( String algorithm , Key secretKey , byte message[] ) throws Exception {
		try {
	        Cipher cipher = Cipher.getInstance(algorithm , "BC");
	        cipher.init( Cipher.ENCRYPT_MODE, secretKey);
	        byte result[] = cipher.doFinal(message);
	        return result;
		}
		catch (Exception e) {
			throw new Exception("error at cryptography_encode():" + e.getMessage() );
		}
	}
	
	/******************************************************************************************
	 * cryptography_encode
	 * @param Key (KeyPair||SecretKey)
	 */
	public static byte[] cryptography_decode( String algorithm , Key secretKey , byte message[] ) throws Exception {
		try {
			Cipher cipher = Cipher.getInstance(algorithm , "BC");
	        cipher.init( Cipher.DECRYPT_MODE, secretKey);
	        message = cipher.doFinal(message);
	        return message;
		}
		catch (Exception e) {
			throw new Exception("error at cryptography_decode():" + e.getMessage() );
		}
	}
	
	
}






