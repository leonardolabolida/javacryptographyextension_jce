package symmetric;

import java.security.Provider;
import java.security.Security;
import java.util.Enumeration;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class JCEExample6_BouncyCastle {
	
	public static void main(String[] args) {
		try {
			
			/* PRINT SECURITY PROVIDERS AND ALGORITHMS */
	        Provider providers[] = Security.getProviders();
	        for (int p = 0; p < providers.length; p++) {
	        	System.out.println( "\nProvider:" + providers[p] );
	        	Enumeration keys = providers[p].keys();
	        	Object ok = keys.nextElement();
	        	while( keys.hasMoreElements() ) {
	        		System.out.println("   key:" + ok );
	        		ok = keys.nextElement();
	        	}
	        } 
	        
	        /*
	        BouncyCastle NOT FOUND !!
	        Let's add BC to providers 
	        */
	        
	        Security.insertProviderAt( new BouncyCastleProvider() , 0 ); // bcprov-jdk14-119.jar
	        
	        
			/* PRINT SECURITY PROVIDERS AND ALGORITHMS */
	        providers = Security.getProviders();
	        for (int p = 0; p < providers.length; p++) {
	        	System.out.println( "\nProvider:" + providers[p] );
	        	Enumeration keys = providers[p].keys();
	        	Object ok = keys.nextElement();
	        	while( keys.hasMoreElements() ) {
	        		System.out.println("   key:" + ok );
	        		ok = keys.nextElement();
	        	}
	        }
	        
	        /*
	         Yes!!! BouncyCastle FOUND !!!
 	        */
		}
		catch (Exception e) {
			System.out.println("error at main():" + e.getMessage() );
		}
	}
	
}
