package symmetric;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.bouncycastle.util.encoders.Base64;

public class Utils {
	
	public static void fileWrite(String filename, byte[] content) throws Exception {
		File f = new File(filename);
		FileOutputStream fos = new FileOutputStream(f,false);
		fos.write(content);
		fos.flush();
		fos.close();
	}
	
	public static byte[] fileRead(String filename ) throws Exception {
		File f = new File(filename);
		FileInputStream fis = new FileInputStream(f);
		byte content[] = new byte[(int)f.length()];
		fis.read(content);
		fis.close();
		return content;
	}
	
	
	public static byte[] base64Encode( byte[] content ) throws Exception {
		return Base64.encode(content);
	}
	
	public static byte[] base64Decode( byte[] content ) throws Exception {
		return Base64.decode(content);
	}
	
	
}
