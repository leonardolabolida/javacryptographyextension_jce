package symmetric;

import java.security.Provider;
import java.security.Security;
import java.util.Enumeration;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

/**
 * JCE - Java Cryptography Extension
 * https://docs.oracle.com/javase/7/docs/api/index.html
 * https://docs.oracle.com/javase/7/docs/api/javax/crypto/Cipher.html
 */
public class JCEExample0_ProvidersAlgorithms {
	
	public static void main(String[] args) {
		try {
			
			/* PRINT SECURITY PROVIDERS AND ALGORITHMS */
	        Provider providers[] = Security.getProviders();
	        for (int p = 0; p < providers.length; p++) {
	        	System.out.println( "\nProvider:" + providers[p] );
	        	Enumeration keys = providers[p].keys();
	        	Object ok = keys.nextElement();
	        	while( keys.hasMoreElements() ) {
	        		System.out.println("   key:" + ok );
	        		ok = keys.nextElement();
	        	}
	        } 
	        
		}
		catch (Exception e) {
			System.out.println("error at main():" + e.getMessage() );
		}
	}
	
}
