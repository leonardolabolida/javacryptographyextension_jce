package symmetric;

public class GenKeyCrack {

	private static final int MAX_KEY_SIZE = 15;
	private static int[] key = new int[MAX_KEY_SIZE];
	
	private static String table = " abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%&/()[]{}?-_";
	private static byte tabl[] = table.getBytes(); 
	
	
	
	public static void main(String[] args) {
		try {
			
			// initialize key
			for ( int x=0 ; x<key.length ; x++ ) {
				key[x]=0;
			}
			
			for ( int gen=0; gen<98762900; gen++) {
				addRecursive( key.length-1 ) ;
				printKey();
			}
			
		}
		catch (Exception e) {
			System.out.println("error at main():" + e.getMessage());
		}
	}

	
	private static void addRecursive( int pos ) {
		if ( key[pos]<tabl.length-1 ) {
			key[pos]++;
		}else{
			key[pos]=0;
			addRecursive(pos-1);
		}
	}
	
	private static void printKey( ) {
		/*
		for ( int x=0 ; x<key.length ; x++ ) {
			System.out.print( key[x] + "-" );
		}
		System.out.print("\n");
		*/
		for ( int x=0 ; x<key.length ; x++ ) {
			System.out.print( (char)tabl[key[x]] /*+ "-"*/ ); // TRIM
		}
		System.out.print("\n");
	}
	
}
