package symmetric;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class JCEExample5_TripleDES {
	
	public static void main(String[] args) {
		try {
	        byte message[] = "This is the message that I want to keep secret".getBytes() ;
	        
	        // GENERATE SYMMETRIC KEY 
	        KeyGenerator keygenerator = KeyGenerator.getInstance("TripleDES","SunJCE");
	        SecretKey secretKey = keygenerator.generateKey();
	        System.out.println( " KEY_Algorithm:" + secretKey.getAlgorithm() );
	        System.out.println( " secretKey.getEncoded:" + new String(secretKey.getEncoded()) );
	        
	        // ENCODE
	        Cipher cipher = Cipher.getInstance("TripleDES" , "SunJCE");
	        cipher.init( Cipher.ENCRYPT_MODE, secretKey);
	        byte result[] = cipher.doFinal(message);
	        System.out.println("result:" + new String(result) );
	        
	        // DECODE
	        cipher.init( Cipher.DECRYPT_MODE, secretKey);
	        byte msg[] = cipher.doFinal(result);
	        System.out.println("result2:" + new String(msg) );
	        
		}
		catch (Exception e) {
			System.out.println("error at main():" + e.getMessage() );
		}
	}
	
}
