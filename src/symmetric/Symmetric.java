package symmetric;

public class Symmetric {

	// ########################################################################
	public static void main(String[] args) {
		try {
			String message = "This is going to be my simple and clear message.";
			String password1 = "abcdefghabcdefgh";
			String password2 = "bbcdefghabcdefgh";
			
			Symmetric crp = new Symmetric();

			String p1 = crp.hash(password1);
			String p2 = crp.hash(password2);
			System.out.println("p1:"+p1);
			System.out.println("p2:"+p2);

			// TEST
			String result1 = crp.symetricEncode(password1, message);
			System.out.println(result1);
			
			String result2 = crp.symetricDecode(password1, result1);
			System.out.println(result2);
			
			if ( ! message.equals(result2) ) {
				System.out.println("ERROR: The messages are different.");
			}
			
			// TEST  
			String result3 = crp.symetricEncode(password2, message);
			System.out.println(result3);
			
			String result4 = crp.symetricDecode(password2, result3);
			System.out.println(result4);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// ########################################################################
	public String symetricEncode ( String password, String message ) throws Exception {
		try {
			StringBuffer result = new StringBuffer();
			
			byte[] m = message.getBytes();
			byte[] p = password.getBytes();
			int r = 0; //result
			int w = 2; //walk
			
			for (int x=0; x<m.length; x++) {
				int im = m[x];
				int ip = p[x % p.length] ;
				int wp = p[(x+w) % p.length] ; // password+walk
				r = im + ip + wp;
				w++;
				result.append( r );
				result.append("#");
			}
			
			return new String(result);
		}
		catch (Exception e) {
			throw new Exception ("error at Symmetric.symetricEncode():" + e.getMessage()) ;
		}
	}
	// ########################################################################
	public String symetricDecode ( String password, String message ) throws Exception {
		try {
			StringBuffer result = new StringBuffer();
			
			String m[] = message.split("#");
			byte[] p = password.getBytes();
			int r = 0; //result
			int w = 2; //walk
			
			for (int x=0; x<m.length; x++) {
				int im = Integer.parseInt(m[x]);
				int ip = p[x % p.length] ;
				int wp = p[(x+w) % p.length] ; // password+walk
				r = im - ip - wp;
				w++;
				result.append( (char)r );
			}
			
			return new String(result);
		}
		catch (Exception e) {
			throw new Exception ("error at Symmetric.symetricDecode():" + e.getMessage()) ;
		}
	}
	
	// ######################################################################## hash
	public static String hash(String key) {
		String tableChars = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ@#{[]}";  
		byte bchars[] = tableChars.getBytes();
		
		final int hashSize = 1024;
		int hash[] = new int[hashSize]; // THE HASH
		
		byte k[] = key.getBytes();
		
		for (int x=0; x<hash.length; x++){  // initialize
			hash[x]=0;
		}
		
		int hold = 0;
		for (int x=0; x<hash.length; x++) {  // hashing
			for (int z=0; z<100; z++) {
				hold = hold + k[(z)%k.length];
				hash[x] = hash[x] + ((hold*2)/3)  ;
			}
		}
		
		StringBuffer buff = new StringBuffer();  // convert to tableChars
		for (int x=0; x<hash.length; x++) {
			int i = hash[x];
			buff.append( (char)bchars[i%bchars.length] );
		}

		return new String(buff);
	}

	
	
	
}
